package com.zebra.bussiness.service.impl.extend;

import com.zebra.bussiness.domain.CommodityCategory;
import com.zebra.bussiness.mapper.CommodityCategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.zebra.bussiness.domain.CityInfo;
import com.zebra.bussiness.domain.ProvinceInfo;
import com.zebra.bussiness.mapper.CityInfoMapper;
import com.zebra.bussiness.mapper.ProvinceInfoMapper;
import com.zebra.common.utils.StringUtils;
import com.zebra.system.domain.SysDept;
import com.zebra.system.mapper.SysDeptMapper;

public class CommodityInfoServiceImplExtend extends BaseServiceImplExtend {
    @Autowired
    private CommodityCategoryMapper commodityCategoryMapper;

    protected String getCategoryName(String categoryId) {
        CommodityCategory commodityCategory = commodityCategoryMapper.selectByPrimaryKey(categoryId);
        if (commodityCategory != null)
            return commodityCategory.getCategoryName();
        return null;
    }
}
