package com.zebra.bussiness.controller;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zebra.bussiness.controller.extend.MerchaninfoControllerExtend;
import com.zebra.bussiness.domain.Merchaninfo;
import com.zebra.bussiness.domain.page.MerchantInfoPage;
import com.zebra.bussiness.service.IMerchaninfoService;
import com.zebra.common.annotation.Log;
import com.zebra.common.core.domain.AjaxResult;
import com.zebra.common.core.page.TableDataInfo;
import com.zebra.common.enums.BusinessType;
import com.zebra.common.utils.poi.ExcelUtil;

/**
 * 商户信息Controller
 *
 * @author zebra
 * @date 2020-05-13
 */
@Controller
@RequestMapping("/bussiness/merchantinfo")
public class MerchaninfoController extends MerchaninfoControllerExtend {
    private String prefix = "bussiness/merchantinfo";

    @Autowired
    private IMerchaninfoService merchaninfoService;

    @RequiresPermissions("bussiness:merchantinfo:view")
    @GetMapping()
    public String merchantinfo() {
        return prefix + "/merchantinfo";
    }

    /**
     * 查询商户信息列表
     */
    @RequiresPermissions("bussiness:merchantinfo:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Merchaninfo merchaninfo) {
        startPage();
        List<Merchaninfo> list = merchaninfoService.selectMerchaninfoList(merchaninfo);
        return getDataTable(super.getMerchantInfoPage(list));
    }

    /**
     * 导出商户信息列表
     */
    @RequiresPermissions("bussiness:merchantinfo:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Merchaninfo merchaninfo) {
        List<Merchaninfo> list = merchaninfoService.selectMerchaninfoList(merchaninfo);
        ExcelUtil<MerchantInfoPage> util = new ExcelUtil<MerchantInfoPage>(MerchantInfoPage.class);
        return util.exportExcel(super.getMerchantInfoPage(list), "商户信息");
    }

    /**
     * 新增商户信息
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存商户信息
     */
    @RequiresPermissions("bussiness:merchantinfo:add")
    @Log(title = "商户信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Merchaninfo merchaninfo) {
        return toAjax(merchaninfoService.insertMerchaninfo(merchaninfo));
    }

    /**
     * 修改商户信息
     */
    @GetMapping("/edit/{merchantId}")
    public String edit(@PathVariable("merchantId") String merchantId, ModelMap mmap) {
        Merchaninfo merchaninfo = merchaninfoService.selectMerchaninfoById(merchantId);
        mmap.put("merchaninfo", merchaninfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存商户信息
     */
    @RequiresPermissions("bussiness:merchantinfo:edit")
    @Log(title = "商户信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Merchaninfo merchaninfo) {
        return toAjax(merchaninfoService.updateMerchaninfo(merchaninfo));
    }

    /**
     * 删除商户信息
     */
    @RequiresPermissions("bussiness:merchantinfo:remove")
    @Log(title = "商户信息", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(merchaninfoService.deleteMerchaninfoByIds(ids));
    }
}
